<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<div class="row" style="margin-left:1pc;margin-right:1pc;">
					<h1>DASHBOARD</h1>
					<hr>
					<?php
					$sql = " select * from barang where stok <= 3";
					$row = $config->prepare($sql);
					$row->execute();
					$r = $row->rowCount();
					if ($r > 0) {
					?>
					<?php
						echo "
							<div class='alert alert-warning'>
								<span class='glyphicon glyphicon-info-sign'></span> Ada <span style='color:red'>$r</span> barang yang Stok tersisa sudah kurang dari 3 items. silahkan pesan lagi !!
								<span class='pull-right'><a href='index.php?page=barang&stok=yes'>Tabel Barang <i class='fa fa-angle-double-right'></i></a></span>
							</div>
							";
					}
					?>
					<?php $hasil_barang = $lihat->barang_row(); ?>
					<?php $stok = $lihat->barang_stok_row(); ?>
					<?php $jual = $lihat->jual_row(); ?>
					<div class="row">
						<!--STATUS PANELS -->
						<div class="col-md-4">
							<div class="panel panel-primary">
								<div class="panel-heading" style="background-color: #4287f5">
									<h5><i class="fa fa-desktop"></i> Nama Barang</h5>
								</div>
								<div class="panel-body">
									<center>
										<h1><?php echo number_format($hasil_barang); ?></h1>
									</center>
								</div>
								<div class="panel-footer">
									<h4 style="color:#4287f5;font-size:15px;font-weight:700;"><a href='index.php?page=barang'>Tabel Barang <i class='fa fa-angle-double-right'></i></a></h4>
								</div>
							</div>
							<!--/grey-panel -->
						</div><!-- /col-md-3-->
						<!-- STATUS PANELS -->
						<div class="col-md-4">
							<div class="panel panel-success">
								<div class="panel-heading" style="background-color: #42bff5">
									<h5 style="color: white"><i class="fa fa-desktop"></i> Stok Barang</h5>
								</div>
								<div class="panel-body">
									<center>
										<h1><?php echo number_format($stok['jml']); ?></h1>
									</center>
								</div>
								<div class="panel-footer">
									<h4 style="color:#4287f5;font-size:15px;font-weight:700;"><a href='index.php?page=barang'>Tabel Barang <i class='fa fa-angle-double-right'></i></a></h4>
								</div>
							</div>
							<!--/grey-panel -->
						</div><!-- /col-md-3-->
						<!-- STATUS PANELS -->
						<div class="col-md-4">
							<div class="panel panel-info">
								<div class="panel-heading" style="background-color: #7b42f5">
									<h5 style="color: white"><i class="fa fa-desktop"></i> Telah Terjual</h5>
								</div>
								<div class="panel-body">
									<center>
										<h1><?php echo number_format($jual['stok']); ?></h1>
									</center>
								</div>
								<div class="panel-footer">
									<h4 style="color:#4287f5; font-size:15px;font-weight:700;font-weight:700;"><a href='index.php?page=laporan'>Tabel laporan <i class='fa fa-angle-double-right'></i></a></h4>
								</div>
							</div>
							<!--/grey-panel -->
						</div><!-- /col-md-3-->
					</div>
					<?php

					$role = $_SESSION['admin']['role'];
					if ($role == 0) { ?>
						<div class="row">
							<div class="col-lg-12">
								<h2>Grafik Penjualan</h2>
								<canvas id="myChart" style="width:100%;max-width:800px"></canvas>
							</div>

						</div>
					<?php } ?>
				</div>
			</div>

		</div>
		<div class="clearfix" style="padding-top:18%;"></div>
	</section>
</section>